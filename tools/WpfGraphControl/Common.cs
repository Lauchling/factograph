using System.Windows;
using System.Windows.Media;
using Color = Microsoft.Msagl.Drawing.Color;

namespace Microsoft.Msagl.WpfGraphControl
{
    internal class Common
    {
        internal static Point WpfPoint(Core.Geometry.Point p)
        {
            return new Point(p.X, p.Y);
        }

        internal static Core.Geometry.Point MsaglPoint(Point p)
        {
            return new Core.Geometry.Point(p.X, p.Y);
        }


        public static Brush BrushFromMsaglColor(Color color)
        {
            var avalonColor = new System.Windows.Media.Color {A = color.A, B = color.B, G = color.G, R = color.R};
            return new SolidColorBrush(avalonColor);
        }

        public static Brush BrushFromMsaglColor(byte colorA, byte colorR, byte colorG, byte colorB)
        {
            var avalonColor = new System.Windows.Media.Color {A = colorA, R = colorR, G = colorG, B = colorB};
            return new SolidColorBrush(avalonColor);
        }


        internal static void PositionFrameworkElement(FrameworkElement frameworkElement, Core.Geometry.Point center,
            double scale)
        {
            PositionFrameworkElement(frameworkElement, center.X, center.Y, scale);
        }

        private static void PositionFrameworkElement(FrameworkElement frameworkElement, double x, double y,
            double scale)
        {
            if (frameworkElement == null)
                return;
            frameworkElement.RenderTransform =
                new MatrixTransform(new Matrix(scale, 0, 0, -scale, x - scale * frameworkElement.Width / 2,
                    y + scale * frameworkElement.Height / 2));
        }
    }
}