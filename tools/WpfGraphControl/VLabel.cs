using System;
using System.Collections.Generic;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using System.Windows.Shapes;
using Microsoft.Msagl.Drawing;
using Label = Microsoft.Msagl.Drawing.Label;

namespace Microsoft.Msagl.WpfGraphControl
{
    internal class VLabel : IViewerObject, IInvalidatable
    {
        internal readonly FrameworkElement FrameworkElement;
        private bool markedForDragging;

        public VLabel(Edge edge, FrameworkElement frameworkElement)
        {
            FrameworkElement = frameworkElement;
            DrawingObject = edge.Label;
        }

        private Line AttachmentLine { get; set; }

        public void Invalidate()
        {
            var label = (Label) DrawingObject;
            Common.PositionFrameworkElement(FrameworkElement, label.Center, 1);
            var geomLabel = label.GeometryLabel;
            if (AttachmentLine != null)
            {
                AttachmentLine.X1 = geomLabel.AttachmentSegmentStart.X;
                AttachmentLine.Y1 = geomLabel.AttachmentSegmentStart.Y;

                AttachmentLine.X2 = geomLabel.AttachmentSegmentEnd.X;
                AttachmentLine.Y2 = geomLabel.AttachmentSegmentEnd.Y;
            }
        }

        public DrawingObject DrawingObject { get; }

        public bool MarkedForDragging
        {
            get => markedForDragging;
            set
            {
                markedForDragging = value;
                if (value)
                {
                    AttachmentLine = new Line
                    {
                        Stroke = Brushes.Black,
                        StrokeDashArray = new DoubleCollection(OffsetElems())
                    }; //the line will have 0,0, 0,0 start and end so it would not be rendered

                    ((Canvas) FrameworkElement.Parent).Children.Add(AttachmentLine);
                }
                else
                {
                    ((Canvas) FrameworkElement.Parent).Children.Remove(AttachmentLine);
                    AttachmentLine = null;
                }
            }
        }

        public event EventHandler MarkedForDraggingEvent;
        public event EventHandler UnmarkedForDraggingEvent;


        private IEnumerable<double> OffsetElems()
        {
            yield return 1;
            yield return 2;
        }
    }
}