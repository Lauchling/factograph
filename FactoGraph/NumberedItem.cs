﻿namespace FactoGraph
{
    public class NumberedItem : INamable
    {
        public NumberedItem(Item item, double amount = 0)
        {
            Item = item;
            Amount = amount;
        }

        public double Amount { get; set; }
        public Item Item { get; set; }

        public string Name => Item.Name;
    }
}