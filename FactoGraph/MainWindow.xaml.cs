﻿using System.Collections.Generic;
using System.Windows;
using System.Windows.Controls;

namespace FactoGraph
{
    /// <summary>
    ///     Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private readonly List<Item> items = new List<Item>();
        private readonly GraphManager man;

        private DockPanel Panel = new DockPanel();

        public MainWindow()
        {
            InitializeComponent();
            // this.Content = Panel;
            Logger.SetList(LogList);
            Loaded += MainWindow_Loaded;


            searchField.TextChanged += (sender, args) =>
            {
                if (sender is TextBox box)
                {
                    var content = box.Text;
                    FillItemList(items.FindAll(item => item.Name.Contains(content)));
                }
            };

            DisplayBtn.Click += (sender, args) =>
            {
                var selectedItem = ItemList.SelectedItem;
                if (selectedItem == null) return;
                man.RenderItem(selectedItem.ToString());
                Tabs.SelectedIndex = 0;
            };

            OrphanedList.SelectionChanged += OnItemListSelection;
            ExcessItemList.SelectionChanged += OnItemListSelection;


            SearchBtn.Click += (sender, args) =>
            {
                var selectedItem = ItemList.SelectedItem;
                if (selectedItem == null) return;
                man.SearchIteminGraph(selectedItem.ToString());
            };

            ExpandBtn.Click += (sender, args) => man.ExpandRecipes();

            var store = new RecipeParser("recipies.txt").Parse();

            items = store.Items;
            FillItemList(items);
            man = new GraphManager(GraphPanel, store, OrphanedList, orphanedSearchField, ExcessItemList,
                ExcessSearchField);
            DataContext = man;
            FilterPanel.DataContext = new RecipeSelectorProxy();
        }

        private void OnItemListSelection(object sender, SelectionChangedEventArgs args)
        {
            if (sender is ListView listview)
            {
                if (listview.SelectedItem == null) return;
                man.SearchIteminGraph(listview.SelectedItem.ToString());
            }
        }

        private void MainWindow_Loaded(object sender, RoutedEventArgs e)
        {
        }

        private void FillItemList(List<Item> list)
        {
            ItemList.Items.Clear();
            foreach (var storeItem in list) ItemList.Items.Add(storeItem.Name);
        }
    }
}