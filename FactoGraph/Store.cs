﻿using System.Collections.Generic;

namespace FactoGraph
{
    internal class Store
    {
        public List<Item> Items;
        public List<Recipe> Recipes;

        public Store()
        {
            Items = new List<Item>();
            Recipes = new List<Recipe>();
        }

        public void AddItem(string name)
        {
            var newItem = new Item(name);
            if (Items.Contains(newItem)) return;

            Items.Add(newItem);
        }

        public Item GetItem(string name)
        {
            foreach (var item in Items)
                if (item.Name.Equals(name))
                    return item;

            var newItem = new Item(name);
            Items.Add(newItem);
            return newItem;
        }

        public void AddRecipe(string name, List<(string name, double amount)> ingredients,
            List<(string name, double amount)> products)
        {
            var newRecipe = new Recipe(name);
            foreach (var ingredient in ingredients)
            {
                var ingredientItem = GetItem(ingredient.name);
                newRecipe.Ingredients.Add(new NumberedItem(ingredientItem, ingredient.amount));
                ingredientItem.ConsumingRecipes.Add(newRecipe);
            }

            foreach (var product in products)
            {
                var productItem = GetItem(product.name);
                newRecipe.Products.Add(new NumberedItem(productItem, product.amount));
                productItem.ProducingRecipes.Add(newRecipe);
            }

            Recipes.Add(newRecipe);
        }
    }
}