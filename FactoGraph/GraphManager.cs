﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Threading;
using System.Xml;
using System.Xml.Linq;
using Microsoft.Msagl.Drawing;
using Microsoft.Msagl.WpfGraphControl;
using Microsoft.Win32;
using MinWpfApp.Annotations;

namespace FactoGraph
{
    internal class GraphManager : INotifyPropertyChanged
    {
        private readonly GraphViewer graphViewer;
        private readonly Store store;
        private readonly Panel targetPanel;
        private string status;
        private string zoomItem;
        private ICommand saveCommand;
        private ICommand loadCommand;
        public ICommand SaveCommand =>saveCommand ?? (saveCommand = new RelayCommand( o => SaveGraph()));
        public ICommand LoadCommand => loadCommand ?? (loadCommand = new RelayCommand(o =>LoadGraph()));


        public GraphManager(Panel panel, Store store, ListBox orpahedItemList, TextBox orphanSearchField,
            ListBox excessItemList, TextBox excessSearchField)
        {
            targetPanel = panel;
            graphViewer = new GraphViewer();
            var orphanedItems = new List<string>();
            var excessItems = new List<string>();
            graphViewer.BindToPanel(targetPanel);
            graphViewer.RunLayoutAsync = true;

            this.store = store;
            graphViewer.LayoutStarted += GraphViewer_LayoutStarted;
            graphViewer.LayoutComplete += GraphViewer_LayoutComplete;
            graphViewer.MouseLUp += Handle_Left_Mouse;
            graphViewer.MouseRUp += Handle_Right_Mouse;

            Options = new ItemDrawOptions
            {
                ConsumingRecursiveLevel = 0,
                DisplayConsumingRecipes = false,
                DisplayProducingRecipes = true,
                ProducingRecursiveLevel = 0
            };

            graphViewer.LayoutComplete += (sender, args) =>
            {
                orpahedItemList.Items.Clear();
                orphanedItems.Clear();
                excessItems.Clear();
                excessItemList.Items.Clear();
                foreach (var graphNode in graphViewer.Graph.Nodes)
                {
                    if (!graphNode.InEdges.Any())
                    {
                        orpahedItemList.Items.Add(graphNode.Id);
                        orphanedItems.Add(graphNode.Id);
                    }

                    if (!graphNode.OutEdges.Any())
                    {
                        excessItemList.Items.Add(graphNode.Id);
                        excessItems.Add(graphNode.Id);
                    }
                }
            };


            orphanSearchField.TextChanged += (sender, args) =>
            {
                if (sender is TextBox box)
                {
                    var content = box.Text;

                    orpahedItemList.Items.Clear();
                    foreach (var storeItem in orphanedItems.FindAll(item => item.Contains(content)))
                        orpahedItemList.Items.Add(storeItem);
                }
            };
            excessSearchField.TextChanged += (sender, args) =>
            {
                if (sender is TextBox box)
                {
                    var content = box.Text;

                    excessItemList.Items.Clear();
                    foreach (var storeItem in excessItems.FindAll(item => item.Contains(content)))
                        excessItemList.Items.Add(storeItem);
                }
            };
        }

        public ItemDrawOptions Options { get; }

        public string Status
        {
            get => status;
            set
            {
                status = value;
                OnPropertyChanged(nameof(Status));
            }
        }


        public event PropertyChangedEventHandler PropertyChanged;

        private void Handle_Right_Mouse(object sender, MsaglMouseEventArgs e)
        {
            var target = graphViewer.ObjectUnderMouseCursor;
            if (target == null) return;
            Remove(target.ToString());
        }

        private void Remove(string id)
        {
            var graph = graphViewer.Graph;
            graph.RemoveNode(graph.FindNode(id));
            var orphanNodes = graph.Nodes.Where(graphNode => !graphNode.Edges.Any()).ToList();
            orphanNodes.ForEach(node => graph.RemoveNode(node));
            graphViewer.Graph = graph;
        }

        private void GraphViewer_LayoutStarted(object sender, EventArgs e)
        {
            Status = "Rendering Graph";
        }

        public void ExpandRecipes()
        {
            var graph = graphViewer.Graph;
            var recipelist = graph.Nodes.Select(graphNode => store.Recipes.Find(r => r.Name.Equals(graphNode.Id)))
                .Where(recipe => recipe != null).ToList();

            foreach (var recipe in recipelist) GraphHelper.ExpandRecipe(graph, recipe, Options);
            graphViewer.Graph = graph;
        }

        public void RenderItem(string item)
        {
            if (Options.ClearOldGraph) graphViewer.Graph = null;

            graphViewer.Graph = GraphHelper.RenderItem(store.GetItem(item), Options, graphViewer.Graph);

            zoomItem = item;
        }

        private void GraphViewer_LayoutComplete(object sender, EventArgs e)
        {
            Status = "Finished Rendering";
            if (zoomItem == null) return;
            Task.Run(() =>
            {
                Thread.Sleep(5);
                Application.Current.Dispatcher.BeginInvoke(
                    DispatcherPriority.Background,
                    new Action(() => SearchIteminGraph(zoomItem)));
            });
        }

        public void SearchIteminGraph(string item)
        {
            var node = graphViewer.Graph.FindNode(item);
            if (node == null) return;
            graphViewer.NodeToCenterWithScale(node, Options.ZoomFactor);
        }


        public void Handle_Left_Mouse(object sender, EventArgs e)
        {
            var target = graphViewer.ObjectUnderMouseCursor;
            if (target == null) return;
            if (!Options.EnableClick) return;
            if (target.ToString().StartsWith("Recipe:"))
            {
            }
            else
            {
                RenderItem(target.ToString());
            }
        }

        [NotifyPropertyChangedInvocator]
        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }

        private void SaveGraph()
        {
            var fileDialog = new SaveFileDialog
            {
                Filter = "MSAGL files (*.msagl)|*.msagl",
                FilterIndex = 1,
                RestoreDirectory = true
            };

            var response = fileDialog.ShowDialog();
            if (!response.HasValue || !response.Value) return;

            var filename = fileDialog.FileName;
            graphViewer.Graph.Write(filename);

            //Shrink Linewidth because i can't find the code which extends them
            var doc = XDocument.Load(filename);
            var elementsToUpdate = doc.Descendants()
                .Where(o => o.Name == "LineWidth" && !o.HasElements);
            foreach (var element in elementsToUpdate) element.Value = "1";

            doc.Save(filename);
            File.WriteAllText(filename + ".json", RecipeSelectorProxy.ToJson());
        }

        private void LoadGraph()
        {
            var fileDialog = new OpenFileDialog
            {
                Filter = "MSAGL files (*.msagl)|*.msagl",
                FilterIndex = 1,
                RestoreDirectory = true
            };

            var response = fileDialog.ShowDialog();
            if (!response.HasValue || !response.Value) return;
            try
            {
                graphViewer.Graph = Graph.Read(fileDialog.FileName);
            }
            catch (XmlException ex)
            {
                MessageBox.Show($"Error while loading the XML-File {fileDialog.FileName}! \n\n {ex.Message}",
                    "XML Error",
                    MessageBoxButton.OK,
                    MessageBoxImage.Error);
            }

            
        }
    }
}