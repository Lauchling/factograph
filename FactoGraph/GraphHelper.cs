﻿using System.Collections.Generic;
using System.Linq;
using Microsoft.Msagl.Drawing;

namespace FactoGraph
{
    internal static class GraphHelper
    {
        public static Graph RenderItem(INamable sourceItem, ItemDrawOptions options, Graph graph = null, int recursiveLevel = 0)
        {
            Item item;
            switch (sourceItem)
            {
                case Item realItem:
                    item = realItem;
                    break;
                case NumberedItem numItem:
                    item = numItem.Item;
                    break;
                default:
                    return null;
            }

            if (graph == null) graph = new Graph {Attr = {LayerDirection = LayerDirection.LR}};


            if (options.DisplayConsumingRecipes)
            {
                var consumingRecipes = item.ConsumingRecipes;
                if (options.AskOnMultipleConsumers && consumingRecipes.Count > 1)
                    consumingRecipes = SelectRecipes(options, graph, item.ConsumingRecipes, item, true);
                foreach (var consumingRecipe in consumingRecipes)
                {
                    AddEdge(graph, sourceItem, consumingRecipe, options);
                    foreach (var products in consumingRecipe.Products)
                    {
                        AddEdge(graph, consumingRecipe, products, options);
                        if (recursiveLevel < options.ConsumingRecursiveLevel)
                            RenderItem(products, options, graph, recursiveLevel + 1);
                    }
                }
            }

            if (options.DisplayProducingRecipes)
            {
                var producingRecipes = item.ProducingRecipes;
                if (options.AskOnMultipleProducers && producingRecipes.Count > 1)
                    producingRecipes = SelectRecipes(options, graph, item.ProducingRecipes, item, false);
                foreach (var producingRecipe in producingRecipes)
                {
                    AddEdge(graph, producingRecipe, sourceItem, options);
                    foreach (var ingredient in producingRecipe.Ingredients)
                    {
                        AddEdge(graph, ingredient, producingRecipe, options);
                        if (recursiveLevel < options.ProducingRecursiveLevel)
                            RenderItem(ingredient, options, graph, recursiveLevel + 1);
                    }
                }
            }


            return graph;
        }

        private static List<Recipe> SelectRecipes(ItemDrawOptions options, Graph graph, List<Recipe> recipes, Item item,
            bool isConsuming)
        {
            List<Recipe> producingRecipes;
            var existingRecipes = ExistingRecipes(recipes, graph);
            if (options.SkipAskingOnExistingRecipes && existingRecipes.Count > 0)
                producingRecipes = existingRecipes;
            else
                producingRecipes = RecipeSelectorProxy.SelectRecipes(item, existingRecipes, isConsuming);

            return producingRecipes;
        }

        private static List<Recipe> ExistingRecipes(IList<Recipe> rawRecipes, Graph graph)
        {
            //var result = graph.Nodes.Select(node => node.Id).Intersect(rawRecipes.Select(recipe => recipe.Name));
            var list = rawRecipes.Where(recipe => graph.Nodes.Any(node => node.Id.Equals(recipe.Name))).ToList();
            return rawRecipes.Where(recipe => graph.Nodes.Any(node => node.Id.Equals(recipe.Name))).ToList();
        }

        private static void AddEdge(Graph graph, INamable source, INamable target, ItemDrawOptions options)
        {
            var sourceAdded = AddNode(graph, source, options);
            var targetAdded = AddNode(graph, target, options);

            if (!sourceAdded && !targetAdded && graph.Edges.Any(graphEdge =>
                    graphEdge.Source.Equals(source.Name) && graphEdge.Target.Equals(target.Name))) return;

            graph.AddEdge(source.Name, target.Name);
            if (sourceAdded && options.AlwaysExpandRecipes) ExpandRecipe(graph, source, options);
            if (targetAdded && options.AlwaysExpandRecipes) ExpandRecipe(graph, target, options);

            switch (source)
            {
                case Item iSource when target is Recipe rTraget:
                    var newSource = rTraget.Ingredients.Find(nItem => nItem.Item.Equals(iSource));
                    UpdateItemAmount(graph, newSource, newSource.Amount * -1);
                    break;

                case NumberedItem nSource:
                    UpdateItemAmount(graph, nSource, nSource.Amount * -1);
                    break;
            }

            switch (target)
            {
                case Item iTarget when source is Recipe rSource:
                    var newTarget = rSource.Products.Find(nItem => nItem.Item.Equals(iTarget));
                    UpdateItemAmount(graph, newTarget, newTarget.Amount);
                    break;
                case NumberedItem nTarget:
                    UpdateItemAmount(graph, nTarget, nTarget.Amount);
                    break;
            }
        }

        private static void UpdateItemAmount(Graph graph, INamable item, double difference)
        {
            var node = graph.FindNode(item.Name);
            if (!node.LabelText.Contains(" ")) node.LabelText = $"0 {node.LabelText}";

            var amount = double.Parse(node.LabelText.Split(' ')[0]);
            amount += difference;
            node.LabelText = $"{amount} {item.Name}";
        }

        private static bool AddNode(Graph graph, INamable id, ItemDrawOptions options)
        {
            if (graph.FindNode(id.Name) != null) return false;
            var isRecipe = id.Name.StartsWith("Recipe:");
            var newNode = new Node(id.Name);
            newNode.Attr.FillColor = isRecipe ? options.RecipeColor : options.ItemColor;
            graph.AddNode(newNode);
            return true;
        }

        public static void ExpandRecipe(Graph graph, INamable id, ItemDrawOptions options)
        {
            if (!(id is Recipe recipe)) return;
            foreach (var recipeProduct in recipe.Products) AddEdge(graph, recipe, recipeProduct, options);

            foreach (var recipeIngredient in recipe.Ingredients) AddEdge(graph, recipeIngredient, recipe, options);
        }
    }


    internal class ItemDrawOptions
    {
        public bool DisplayConsumingRecipes { get; set; } = true;
        public bool DisplayProducingRecipes { get; set; } = true;

        public int ProducingRecursiveLevel { get; set; } = 0;
        public int ConsumingRecursiveLevel { get; set; } = 0;

        public bool ClearOldGraph { get; set; } = false;

        public bool EnableClick { get; set; } = true;

        public double ZoomFactor { get; set; } = 1.0;

        public Color ItemColor { get; set; } = Color.LightGreen;
        public Color RecipeColor { get; set; } = Color.Beige;

        public bool AskOnMultipleConsumers { get; set; } = true;
        public bool AskOnMultipleProducers { get; set; } = true;
        public bool AlwaysExpandRecipes { get; set; } = true;
        public bool SkipAskingOnExistingRecipes { get; set; } = true;
    }
}