﻿namespace FactoGraph
{
    internal interface INamable
    {
        string Name { get; }
    }
}