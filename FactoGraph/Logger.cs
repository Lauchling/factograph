﻿using System.Windows.Controls;

namespace FactoGraph
{
    internal static class Logger
    {
        public static ListView View { get; private set; }

        public static void SetList(ListView listView)
        {
            View = listView;
            Log("Log started");
        }

        public static void Log(string text)
        {
            View.Items.Add(text);
        }
    }
}