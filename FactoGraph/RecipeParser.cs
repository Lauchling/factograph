﻿using System.Collections.Generic;
using System.IO;

namespace FactoGraph
{
    internal class RecipeParser
    {
        private readonly string filename;

        public RecipeParser(string filename)
        {
            this.filename = filename;
        }


        public Store Parse()
        {
            var store = new Store();
            Logger.Log("Start Parsing");
            var lines = File.ReadAllLines(filename);
            Logger.Log("File Read");
            foreach (var line in lines)
            {
                var stuff = line.Split('@');

                var recipeName = stuff[0];
                var ingredients = GetItems(stuff[3]);
                var products = GetItems(stuff[2]);
                if (recipeName.Contains("barrel")) continue;
                store.AddRecipe(recipeName, ingredients, products);
            }

            return store;
        }

        private List<(string name, double amount)> GetItems(string input)
        {
            if (!input.Contains(":")) return new List<(string name, double amount)>();
            if (!input.Contains(",")) return new List<(string name, double amount)>();
            var list = input.Split('=');
            var itemPairs = list[1].Split(',');

            var newList = new List<(string name, double amount)>();
            foreach (var pair in itemPairs)
            {
                if (!pair.Contains(":")) continue;
                var newSplit = pair.Split(':');
                newList.Add((newSplit[1], double.Parse(newSplit[0])));
            }

            return newList;
        }
    }
}