﻿using System.Collections.Generic;
using System.Linq;
using System.Windows;

namespace FactoGraph
{
    /// <summary>
    ///     Interaktionslogik für RecipeSelector.xaml
    /// </summary>
    public partial class RecipeSelector : Window
    {
        private readonly List<Recipe> originalList;
        private readonly List<Recipe> recipeList;

        public RecipeSelector(Item item, bool isConsuming, IEnumerable<Recipe> existingRecipes)
        {
            InitializeComponent();
            var action = isConsuming ? "consuming" : "producing";
            Title.Content = $"Item {item.Name} has multiple {action} Recipes! Select:";
            originalList = isConsuming ? item.ConsumingRecipes : item.ProducingRecipes;
            recipeList = new List<Recipe>();
            foreach (var recipe in originalList)
            {
                var ingredientsNames = "";
                foreach (var ingredient in recipe.Ingredients) ingredientsNames += $"{ingredient.Name}, ";

                if (ingredientsNames.Length > 2)
                    ingredientsNames = ingredientsNames.Substring(0, ingredientsNames.Length - 2);

                var productNames = "";
                foreach (var product in recipe.Products) productNames += $"{product.Name}, ";

                if (productNames.Length > 2) productNames = productNames.Substring(0, productNames.Length - 2);
                var alreadyExists = existingRecipes.Contains(recipe);

                listView.Items.Add(new
                {
                    Recipe = recipe.Name, Ingredients = ingredientsNames, Products = productNames,
                    AlreadyExists = alreadyExists
                });
            }
        }

        public static List<Recipe> SelectRecipes(Item item, bool isConsuming, IEnumerable<Recipe> existingRecipes)
        {
            var selector = new RecipeSelector(item, isConsuming, existingRecipes);
            selector.ShowDialog();
            return selector.recipeList;
        }

        private void DoneBtn_Click(object sender, RoutedEventArgs e)
        {
            foreach (var t in listView.SelectedItems)
            {
                dynamic typ = t;
                recipeList.Add(originalList.Find(recipe => recipe.Name.Equals(typ.Recipe)));
            }

            Close();
        }
    }
}