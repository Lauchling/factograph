﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Windows;
using System.Windows.Input;
using Microsoft.Win32;
using MinWpfApp;
using Newtonsoft.Json;
using Formatting = System.Xml.Formatting;

namespace FactoGraph
{
    public class RecipeSelectorProxy
    {
        private static readonly Dictionary<Item, List<Recipe>> selectedConsumingRecipes =
            new Dictionary<Item, List<Recipe>>();

        private static readonly Dictionary<Item, List<Recipe>> selectedProducingRecipes =
            new Dictionary<Item, List<Recipe>>();

        private ICommand _clickCommand;

        public RecipeSelectorProxy()
        {
            OnDictionaryChanged += OnDictionaryUpdate;
            selectedItems = new ObservableCollection<object>();
            foreach (var item in selectedConsumingRecipes.Keys)
                selectedItems.Add(new SelectedRecipeItem($"Consuming {item.Name}", item,
                    selectedConsumingRecipes[item].Count, item.ConsumingRecipes.Count, selectedConsumingRecipes));
            foreach (var item in selectedProducingRecipes.Keys)
                selectedItems.Add(new SelectedRecipeItem($"Producing {item.Name}", item,
                    selectedProducingRecipes[item].Count, item.ProducingRecipes.Count, selectedProducingRecipes));
        }

        public ICommand RemoveClickCommand =>
            _clickCommand ?? (_clickCommand = new RelayCommand<SelectedRecipeItem>(OnRemoveClick));

        public ObservableCollection<object> selectedItems { get; set; }
        private static event DictionaryChanged OnDictionaryChanged;

        private bool SelectionNotEmpty()
        {
            return selectedItems.Count > 0;
        }

        private void OnRemoveClick(SelectedRecipeItem selectedItem)
        {
            selectedItem.List.Remove(selectedItem.Item);
            selectedItems.Remove(selectedItem);
        }


        private void OnDictionaryUpdate(object sender, object e)
        {
            if (e is Item item && sender is Dictionary<Item, List<Recipe>> bufferList)
            {
                var description = $"Producing {item.Name}";
                var numRec = bufferList == selectedConsumingRecipes
                    ? item.ConsumingRecipes.Count
                    : item.ProducingRecipes.Count;
                if (bufferList == selectedConsumingRecipes) description = $"Consuming {item.Name}";
                selectedItems.Add(new SelectedRecipeItem(description, item, bufferList[item].Count, numRec,
                    bufferList));
            }
        }

        public static List<Recipe> SelectRecipes(Item item, bool isConsuming = true)
        {
            return SelectRecipes(item, new List<Recipe>(), isConsuming);
        }

        public static Dictionary<string, string> StringifyDictionary(Dictionary<Item, List<Recipe>> source)
        {
            var result = new Dictionary<string, string>();
            foreach (var keyValuePair in source)
            {
                var key = keyValuePair.Key.Name;
                var rawValue = new List<string>();
                foreach (var recipe in keyValuePair.Value)
                {
                    rawValue.Add(recipe.Name);
                }
                result.Add(key, JsonConvert.SerializeObject(rawValue));
            }
            return result;
        }

        public static string ToJson()
        {
            return JsonConvert.SerializeObject(StringifyDictionary(selectedProducingRecipes), Newtonsoft.Json.Formatting.Indented);
        }

        public static List<Recipe> SelectRecipes(Item item, IEnumerable<Recipe> existingRecipes,
            bool isConsuming = true)
        {
            var bufferList = isConsuming ? selectedConsumingRecipes : selectedProducingRecipes;
            if (!bufferList.ContainsKey(item))
            {
                bufferList.Add(item, RecipeSelector.SelectRecipes(item, isConsuming, existingRecipes));
                OnDictionaryChanged?.Invoke(bufferList, item);
            }

            return bufferList[item];
        }

        private delegate void DictionaryChanged(object sender, object e);
    }

    public class SelectedRecipeItem
    {
        public SelectedRecipeItem(string description, Item item, int numSel, int numRec,
            Dictionary<Item, List<Recipe>> list)
        {
            Description = description;
            Item = item;
            NumSel = numSel;
            NumRec = numRec;
            List = list;
        }

        public string Description { get; }
        public Item Item { get; }
        public int NumSel { get; }
        public int NumRec { get; }
        public Dictionary<Item, List<Recipe>> List { get; }
    }
}