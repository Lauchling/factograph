﻿using System.Collections.Generic;

namespace FactoGraph
{
    public class Item : INamable
    {
        public readonly List<Recipe> ConsumingRecipes;
        public readonly List<Recipe> ProducingRecipes;

        public Item(string name)
        {
            Name = name;
            ConsumingRecipes = new List<Recipe>();
            ProducingRecipes = new List<Recipe>();
        }

        public string Name { get; }

        public override bool Equals(object obj)
        {
            if (obj is Item otherItem) return Name.Equals(otherItem.Name);

            return false;
        }

        public override int GetHashCode()
        {
            return Name.GetHashCode();
        }
    }
}