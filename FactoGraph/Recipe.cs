﻿using System.Collections.Generic;

namespace FactoGraph
{
    public class Recipe : INamable
    {
        private readonly string name;

        public List<NumberedItem> Ingredients;
        public List<NumberedItem> Products;

        public Recipe(string name)
        {
            this.name = name;
            Ingredients = new List<NumberedItem>();
            Products = new List<NumberedItem>();
        }

        public string Name => $"Recipe:{name}";

        public override bool Equals(object obj)
        {
            if (obj is Recipe otherItem) return Name.Equals(otherItem.Name);

            return false;
        }

        public override int GetHashCode()
        {
            return Name.GetHashCode();
        }
    }
}